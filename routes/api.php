<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Signature;
use Illuminate\Support\Facades\Storage;

Route::post('signature', function (Request $request) {


    $base64_image = $request->image;


    if (preg_match('/^data:image\/(\w+);base64,/', $base64_image)) {

        $image = substr($base64_image, strpos($base64_image, ',') + 1);
        $image = base64_decode($image);

        $nama =  rand() . ".png";
        Storage::put("public/signatures/$nama", $image);

        $signature = new Signature();
        $signature->image_url = "/storage/signatures/$nama";
        $signature->image_path = "public/signatures/$nama";
        $signature->save();

        return response()->json([
            'message' => 'Berhasil',
            'data' => $signature
        ]);
    }


    return response()->json([
        'message' => 'Gagal',
        'data'=> $request->all()
    ], 422);
});
